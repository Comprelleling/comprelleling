# Comprelleling

Comprelleling is an open-source distributed DNN training framework that takes full advantage of Gradient Compression Algorithms, thus achieves higher performance.

To realize the Comprelleling, we develop 2 libraries: `CG-allreduce`(based on [Horovod](https://github.com/uber/horovod)) and `OpenGC`.

`CG-allreduce` is for synchronizing compressed gradients among all nodes. `OpenGC` is for providing independent highly optimized implementation of many state-of-art Compression algorithms(e.g. TernGrad, DGC)

## Why Choose Comprelleling

Gradient synchronization among training nodes is expensive and can lead to poor scalability and low resource utilization in data parallel distributed DNN training. To mitigate this bottleneck, a number of gradient 
Large gradient synchronization lowers the scalablity of distributed DNN training, therefore many gradient compression algorithms are proposed from community. However, these algorithms often overlook the system challenge in implementation and ease of use. For example, enabling TBQ in MXNet achieves very slight improvements of training speed. To address these challenges, we propose the first generic framework, `Comprelleling`, to support fast compressiong-enabled data parallel DNN training. Following are the contribution we make.

1. Achieves a speedup of up to 12x across both communication- and computation-intensive DNN models used in computer vision and natural language processing fields, compared to the industry-adopted TBQ solution as well as MXNet and TensorFlow.
2. Create a plug-and-play scheme for various gradient compressiong algorithms to be easily integrated into DNN systems with little programming burden.
3. Build a fully-functional prototype atop of MXNet and TensorFlow.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
[install cuda 10.1]
[install cuDNN 7.5.1]
[install MPI 3.1.2]
[install nccl 2.4.2]
```

### Installing

Install [Anaconda3](https://repo.continuum.io/archive/)

```
[download anaconda3]
bash Anaconda3-5.2.0-Linux-x86_64.sh
```

Install [bazel](https://github.com/bazelbuild/bazel/releases)
```
[Download bazel]
bash bazel-0.24.1-installer-linux-x86_64.sh
```

Install TensorFlow( if you are using TensorFlow)
```
cd ~
git clone https://github.com/tensorflow/tensorflow
cd tensorflow
git checkout r1.14
./configure
    #[only enable cuda support]
bazel build --config=opt --config=cuda --cxxopt="-D_GLIBCXX_USE_CXX11_ABI=0" //tensorflow/tools/pip_package:build_pip_package
mkdir ~/tmp
./bazel-bin/tensorflow/tools/pip_package/build_pip_package ~/tmp/tensorflow_pkg
pip install ~/tmp/tensorflow_pkg/tensorflow-1.14.1-cp36-cp36m-linux_x86_64.whl

```

Install MXNet( if you are using MXNet)
```
# We changed some source codes of MXNet1.5.0 to integrate with CG-allreduce, so we provide the MXNet's source code and some scripts in this project 

# Clone the Comprelleling project first
cd comprelleling/code/mxnet-1.5.0
bash ./compile.sh
cd python
pip install -e .
```

Install Comprelleling
```
#download
cd ~
git clone https://gitlab.com/Comprelleling/comprelleling
ln -s ~/comprelleling/code/CG-allreduce/horovod ./horovod # only for tensorflow

# compile compression operations for tensorflow
cd comprelleling/code/OpenGC/tensorflow-compression-implementation
make -j

# compile compression operations for mxnet
cp ~/comprelleling/code/OpenGC/mxnet-compression-implementation/* ~/comprelleling/code/mxnet-1.5.0/src/operator/contrib/ -r
cd ~/comprelleling/code/mxnet-1.5.0
bash ./compile.sh

# install CG-allreduce
cd ~/comprelleling/code/CG-allreduce/horovod
bash ./install.sh #turn off HOROVOD_WITH_TENSORFLOW/HOROVOD_WITH_MXNET for your own configurations.
```

Debug
```
#No module named 'keras_preprocessing'
pip install keras_preprocessing

#Cannot unisntall 'wrapt'. It is a distutils installed project and thus we cannot accurately determine which files belong to it which would lead  to only a partial uninstall
pip install -U --ignore-installed wrapt
pip install ~/tmp/tensorflow_pkg/tensorflow-1.14.1-cp36-cp36m-linux_x86_64.whl
```



## Running the tests
### MXNet
```
cd ~/comprelleling/code/CG-allreduce/horovod/horovod-mxnet

# train on multi machines, we provide some scripts to start the training conveniently, see script help for details
python resource_usage.py --numprocess 2 --servers localhost:1,egpu2:1 --model vgg19 --comp-threshold 262144 --comp-alg tbq --horovodrun
```
### TensorFlow
```
cd ~/comprelleling/code/CG-allreduce/horovod/examples/benchmarks
#copy binaries of operations
cp ~/comprelleling/code/OpenGC/tensorflow-compression-implementation/*.so
cp ~/comprelleling/code/OpenGC/tensorflow-compression-implementation/*.o

# train on multi machines
cp ~/comprelleling/code/OpenGC/tensorflow-compression-implementation/*.so ./
cp ~/comprelleling/code/OpenGC/tensorflow-compression-implementation/*.o ./
horovodrun -np 2 -H localhost:1,egpu2:1 python ./scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py --model vgg19 --batch_size 16 --variable_update horovod --threshold 262144

```

## License

* [to implement]

<!-- This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details -->

<!-- ## Acknowledgments

* to implement -->

