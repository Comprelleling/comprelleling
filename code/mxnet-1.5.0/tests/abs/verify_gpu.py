import numpy as np
import mxnet as mx
import time

if __name__ == '__main__':
    a = [(i) % 300000 for i in range(2**27)]
    a = np.array(a)
    print("a prepared!")
    b = mx.nd.array(a,ctx=mx.context.gpu(0))
    c = mx.nd.array(a,ctx=mx.context.gpu(0))
    print("b prepared!")
    print("b.ctx:",b.context)
    mx.nd.contrib.abs(data=b,out=c)
    c.wait_to_read()
    t1 = time.time()
    mx.nd.contrib.abs(data=b,out=c)
    c.wait_to_read()
    t2 = time.time()

    #d = c.asnumpy()
    #print("d[:20]",d[:20])
    print("cost time (s):",t2-t1)
    exit()


    a = [i-10 for i in range(20)]
    a = np.array(a)
    #a = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,12,12])
    print("origin data:",a)
    b = mx.nd.array(a)
    print("input data before abs:", b)
    c = mx.nd.contrib.abs(data=b)
    print("output data:",c)
    print("input data after abs:",b)






