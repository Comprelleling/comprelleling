import numpy as np
import mxnet as mx
import time

output_csv = r"dgc_cpu_single_thread_wait_all.csv"

def binary(num):
    import struct
    return ''.join(bin(c).replace('0b', '').rjust(8, '0') for c in struct.pack('!f', num))

def float32_to_int32(num):
    b = binary(num)
    if b[0]=='0':
        return int(b,2)
    else:
        return -int(b[1:],2)
def test_time(input_size, s_percent=0.01, sample_rate = 0.001):
    print("testing:\tinput_size={}\ts_percent={}\tsample_rate={}".format(input_size,s_percent,sample_rate))
    total_time = 0
    total_cnt = 0
    total_time_reverse = 0

    input_data0 = np.random.randn(input_size)
    input_data = mx.nd.array(input_data0,ctx=mx.context.cpu_pinned())
    output_size = 2*input_size+2
    output = mx.nd.zeros(shape=(output_size),dtype='float32',ctx=mx.context.cpu_pinned())
    input_reverse = mx.nd.zeros(shape=(input_size),dtype='float32',ctx=mx.context.cpu_pinned())
    while total_cnt < 10:
        total_cnt += 1
        t1 = time.time()
        mx.nd.contrib.dgc(data=input_data,s_percent=s_percent,sample_rate=sample_rate,out=output)
        output.wait_to_read()
        t2 = time.time()
        o2 = output.asnumpy()
        true_size = float32_to_int32(o2[-1])
        o2 = o2[:true_size*2+1]
        original_size = float32_to_int32(o2[-1])
        o2 = mx.nd.array(o2,ctx=mx.context.cpu_pinned())
        t3 = time.time()
        mx.nd.contrib.dgcr(data=o2,original_size=original_size,out=input_reverse)
        input_reverse.wait_to_read()
        t4 = time.time()
        total_time = total_time + t2 - t1
        total_time_reverse = total_time_reverse  + t4 - t3;
        continue
        output = output.asnumpy()
        print("start checking correctness...")
        print("len(input_data0)={}".format(len(input_data0)))
        print("len(output)={}".format(len(output)))
        assert(len(input_data0)==len(output))
        print("input_data0:",input_data0)
        print("output:",output)
        cnt = 0
        for i in range(len(output)):
            if i % 100000 == 0:
                print(i,'...')
            if output[i]!=0:
                cnt+=1
                assert(abs(input_data0[i] - output[i])<0.01)
                #assert(input_data0[i] == output[i])
        print("check pass! true_size={}\tcnt={}".format(true_size,cnt))

    average_time = total_time / total_cnt
    average_time_reverse = total_time_reverse / total_cnt
    with open(output_csv,'a') as f:
        line = ["dgc",input_size,s_percent,sample_rate,total_time,total_cnt,average_time]
        line = [str(v) for v in line]
        line = ','.join(line)
        f.write(line+'\n')
        line = ["dgcr",input_size,s_percent,sample_rate,total_time_reverse,total_cnt,average_time_reverse]
        line = [str(v) for v in line]
        line = ','.join(line)
        f.write(line+'\n')

    print("average_time={}".format(average_time))
    print("average_time_reverse={}".format(average_time_reverse))


 

if __name__ == '__main__':
    with open(output_csv, 'w') as f:
        attr = ["operator","input_size","s_percent","sample_rate","total_time","total_cnt","average_time"]
        line = ','.join(attr)
        f.write(line+'\n')


    max_size = 134217728
    input_size = 32
    while input_size <= max_size:
        for s_percent in [0.01,0.001]:
            test_time(input_size,s_percent=s_percent)
            input_size*=2
   
