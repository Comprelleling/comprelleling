#include "./dgc_thrust-inl.h"
#include <thrust/execution_policy.h>  //thrust::device

namespace mxnet {
namespace op {

NNVM_REGISTER_OP(_contrib_dgcr_thrust)
.set_attr<FCompute>("FCompute<gpu>", DGCR_THRUSTOpForward<gpu, thrust::cuda_cub::par_t::stream_attachment_type>)
;

NNVM_REGISTER_OP(_contrib_dgc_thrust)
.set_attr<FCompute>("FCompute<gpu>", DGC_THRUSTOpForward<gpu, thrust::cuda_cub::par_t::stream_attachment_type>)
;

}  // namespace op
}  // namespace mxnet
