#include "dgcg_op-inl.h"
#include <mxnet/operator_util.h>
#include <vector>
#include "../mshadow_op.h"
#include "../mxnet_op.h"
#include "../operator_common.h"
#include "../elemwise_op_common.h"
#include "../tensor/init_op.h"
#include <cmath>

#include <cuda.h>
#include <curand.h>

#include <cuda_runtime.h>
#include <curand_kernel.h>
#include <device_launch_parameters.h>


#include <sys/time.h>  //gettimeofday

#include "dgcg_mergesort.h"

#define CURAND_ORDERING_PSEUDO_SEEDED

namespace mxnet{
namespace op{

#define to_array(obj,to_type,from_type) ((to_type*)(obj.dptr<from_type>()))
#define ceil_divide(a,b) ((a+b-1)/b)

struct DGCG_sampling_forward{
    MSHADOW_XINLINE static void Map(int i, float* in_float, float* out_float, size_t input_size){
        size_t index = ((unsigned int*)out_float)[i] % input_size;
        out_float[i] = std::abs(in_float[index]);
    }
};

struct DGCG_generate_forward{
    MSHADOW_XINLINE static void Map(int i, float* in_float, float* out_float, size_t input_size){
        size_t index = ((unsigned int*)out_float)[i] % input_size;
        out_float[i] = std::abs(in_float[index]);
    }
};


struct DGCG_filter_forward{
    MSHADOW_XINLINE static void Map(
        int i, 
        float* in_float, 
        float* out_float,
        float threshold,
        int32_t input_size,
        int32_t original_num_per_thread,
        int32_t result_num_per_thread
    ){
        int32_t input_index = i*original_num_per_thread;
        // int32_t* out_int32_t = static_cast<int32_t*>(out_float);
        int32_t* out_int32_t = (int32_t*)out_float;
        // warning: calling a __host__ function from a __host__ __device_- function is not allowed
        // int32_t input_end_index = std::max((i+1)*original_num_per_thread, input_size);
        int32_t input_end_index = (i+1)*original_num_per_thread < input_size 
            ? (i+1)*original_num_per_thread
            : input_size;
        int32_t output_index = (result_num_per_thread*i)<<1;
        int32_t output_end_index = ((i+1) * result_num_per_thread)<<1;
        for (;input_index < input_end_index; input_index++){
            if (std::abs(in_float[input_index]) >= threshold){
                // out_float[out_index++] = in_float[input_index];
                out_float[output_index++] = in_float[input_index];
                out_int32_t[output_index++] = input_index;
                if (output_index == output_end_index){
                    break;
                }
            }
        }
        // no need any more, have filled with 0 before.
        // for (;output_index < output_end_index;){
        //     out_float[output_index++] = 0;
        //     out_int32_t[output_index++] = 0;
        // }
    }
};

struct DGCRG_forward{
    MSHADOW_XINLINE static void Map(
        int i,
        float* in_float,
        int32_t* in_int32_t,
        float* out_float
    ){
        out_float[in_int32_t[(i<<1)+1]] = in_float[i<<1];
    }
};

#define GET_COST_TIME_BY_US(t1,t2) (t2.tv_sec-t1.tv_sec)*1e6+(t2.tv_usec-t1.tv_usec)/1.0 

template<typename xpu>
void DGCGOpForward(const nnvm::NodeAttrs& attrs,
                        const OpContext& ctx,
                        const std::vector<TBlob>& inputs,
                        const std::vector<OpReqType>& req,
                        const std::vector<TBlob>& outputs) {
    using namespace mxnet_op;
    const DGCGParam& param = nnvm::get<DGCGParam>(attrs.parsed);
    CHECK_EQ(inputs.size(), 1U);
    CHECK_EQ(outputs.size(), 1U);
    CHECK_EQ(req.size(), 1U);

    const TBlob& in_data = inputs[0];
    const TBlob& out_data = outputs[0];
    auto in_float = to_array(in_data,float,float);
    auto out_float = to_array(out_data,float,float);

    // size_t sample_cnt = std::max((size_t)(in_data.Size()*param.sample_rate),(size_t)(10/param.s_percent));
    // if (sample_cnt <= in_data.Size()){
    //     //sample
    // }
    // else{
    //     //do not sort
    // }
    // unsigned int SHARED_SIZE_LIMIT = 1024;
    size_t sample_cnt = (size_t)(in_data.Size()*param.sample_rate);
    sample_cnt = ((sample_cnt + SHARED_SIZE_LIMIT - 1)/SHARED_SIZE_LIMIT)*SHARED_SIZE_LIMIT;
    mshadow::Stream<xpu> *s = ctx.get_stream<xpu>();
    struct timeval t1,t1_4,t1_5,t2,t3,t4;
    dim3 threads(16,1);
    dim3 grid((1+threads.x-1)/threads.x,1);
    gettimeofday(&t1,NULL); 

    curandGenerator_t gen;
    curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT);
    curandSetPseudoRandomGeneratorSeed(gen,time(0));
    cudaDeviceSynchronize();
    gettimeofday(&t1_4,NULL);
    
    curandGenerate(gen,(unsigned int*)out_float,sample_cnt);
    cudaDeviceSynchronize();
    gettimeofday(&t1_5,NULL);

    Kernel<DGCG_sampling_forward,xpu>::Launch_lite(
        s, sample_cnt, in_float, out_float, in_data.Size()
    );

    cudaDeviceSynchronize();
    gettimeofday(&t2,NULL);
    // std::cout<<"sorting it!"<<std::endl;
    //note: sort cnt must be 1024's beishu
    dgcSort<float>(out_float,out_data.Size(),sample_cnt,0);
    int32_t threshold_index = (int32_t)(sample_cnt*(2+param.s_percent));
    float threshold;
    cudaMemcpy(&threshold,out_float+threshold_index,sizeof(float),cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
    gettimeofday(&t3,NULL);
    // std::cout<<"selected threshold by sampling: " << threshold<<std::endl;
    // assuming input N = 2^20 = 1048576, s_percent = 0.01, sample_rate = 0.001, result_num_per_thread = 10
    // result_cnt = ceil(1048576*0.01) = 10486
    // result_thread_num = (10486+10-1)/10 = 1049
    // original_num_per_thread = (1048576+1049-1)/1049 = 1000
    // output_size = 1049 * 10 = 10490
    int32_t result_cnt = static_cast<int32_t>(std::ceil(in_data.Size() * param.s_percent));
    int32_t result_num_per_thread = param.result_num_per_thread; // default is 10
    int32_t result_thread_num = ceil_divide(result_cnt, result_num_per_thread);  //14
    int32_t output_size = result_thread_num * result_num_per_thread;
    cudaMemset(out_float, 0, sizeof(float)*2*output_size); // set it to 0.

    int32_t original_num_per_thread = ceil_divide(in_data.Size(),result_thread_num); //134134/14 = 9581.07, original_num_per_thread = 9582
    Kernel<DGCG_filter_forward,xpu>::Launch_lite(
        s, 
        result_thread_num, 
        in_float, 
        out_float,
        threshold, 
        in_data.Size(), 
        original_num_per_thread, 
        result_num_per_thread
    );
    cudaDeviceSynchronize();
    gettimeofday(&t4,NULL);

    std::cout<<"t1~t1.4: init rand time::\t" <<GET_COST_TIME_BY_US(t1,t1_4)<<std::endl;
    std::cout<<"t1.4~t1.5: gene rand time:\t" <<GET_COST_TIME_BY_US(t1_4,t1_5)<<std::endl;
    std::cout<<"t1.5~t2: sampling time:\t" <<GET_COST_TIME_BY_US(t1_5,t2)<<std::endl;
    std::cout<<"t2~t3: sorting time:\t" <<GET_COST_TIME_BY_US(t2,t3)<<std::endl;
    std::cout<<"t3~t4: filtering time:\t"<<GET_COST_TIME_BY_US(t3,t4)<<std::endl;
    
    
}



template<typename xpu>
void DGCRGOpForward(const nnvm::NodeAttrs& attrs,
                        const OpContext& ctx,
                        const std::vector<TBlob>& inputs,
                        const std::vector<OpReqType>& req,
                        const std::vector<TBlob>& outputs) {
    using namespace mxnet_op;
    const DGCRGParam& param = nnvm::get<DGCRGParam>(attrs.parsed);
    CHECK_EQ(inputs.size(), 1U);
    CHECK_EQ(outputs.size(), 1U);

    const TBlob& in_data = inputs[0];
    const TBlob& out_data = outputs[0];
    auto in_float = to_array(in_data,float,float);
    auto in_int32_t = to_array(in_data,int32_t,float);
    auto out_float = to_array(out_data,float,float);

    cudaMemset(out_float,0,sizeof(float)*out_data.Size());

    mshadow::Stream<xpu> *s = ctx.get_stream<xpu>();
    Kernel<DGCRG_forward,xpu>::Launch_lite(
        s,
        param.quantized_num,
        in_float,
        in_int32_t,
        out_float
    );
}


DMLC_REGISTER_PARAMETER(DGCGParam);
DMLC_REGISTER_PARAMETER(DGCRGParam);

NNVM_REGISTER_OP(_contrib_dgcg)
    .set_attr_parser(ParamParser<DGCGParam>)
    .set_num_inputs(1)
    .set_num_outputs(1)
    .set_attr<nnvm::FListInputNames>("FListInputNames",
        [](const NodeAttrs& attrs) {
            return std::vector<std::string>{"data"};
    })
    .set_attr<mxnet::FInferShape>("FInferShape", DGCGOpShape)
    .set_attr<nnvm::FInferType>("FInferType", DGCGOpType)
    .set_attr<FCompute>("FCompute<gpu>", DGCGOpForward<gpu>)
    .set_attr<nnvm::FInplaceOption>("FInplaceOption",
        [](const NodeAttrs& attrs) {
            return std::vector<std::pair<int, int> >{{0, 0}};
    })
    .add_argument("data", "NDArray-or-Symbol", "Input ndarray")
    .add_arguments(DGCGParam::__FIELDS__())
    ;

NNVM_REGISTER_OP(_contrib_dgcrg)
    .set_attr_parser(ParamParser<DGCRGParam>)
    .set_num_inputs(1)
    .set_num_outputs(1)
    .set_attr<nnvm::FListInputNames>("FListInputNames",
        [](const NodeAttrs& attrs) {
            return std::vector<std::string>{"data"};
    })
    .set_attr<mxnet::FInferShape>("FInferShape", DGCRGOpShape)
    .set_attr<nnvm::FInferType>("FInferType", DGCRGOpType)
    .set_attr<FCompute>("FCompute<gpu>", DGCRGOpForward<gpu>)
    .set_attr<nnvm::FInplaceOption>("FInplaceOption",
        [](const NodeAttrs& attrs) {
            return std::vector<std::pair<int, int> >{{0, 0}};
    })
    .add_argument("data", "NDArray-or-Symbol", "Input ndarray")
    .add_arguments(DGCRGParam::__FIELDS__())
    ;

} // namespace op
} // namespace mxnet