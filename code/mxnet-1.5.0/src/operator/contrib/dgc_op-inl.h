/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*!
 * \file quad_function-inl.h
 * \brief Operator implementing quadratic function.
 * For using as an example in the tutorial of adding operators
 * in MXNet backend.
 */
#ifndef MXNET_OPERATOR_CONTRIB_DGC_OP_INL_H_
#define MXNET_OPERATOR_CONTRIB_DGC_OP_INL_H_

#include <mxnet/operator_util.h>
#include <vector>
#include "../mshadow_op.h"
#include "../mxnet_op.h"
#include "../operator_common.h"
#include "../elemwise_op_common.h"
#include "../tensor/init_op.h"

#include <iostream>
#include <chrono>
#include <random>
#include <cmath> //abs
#include <algorithm> // sort



namespace mxnet {
namespace op {

struct DGCRParam : public dmlc::Parameter<DGCRParam> {
  int32_t original_size;
  DMLC_DECLARE_PARAMETER(DGCRParam){
    DMLC_DECLARE_FIELD(original_size)
      .describe("The size of data before sparsity.");
  };
};

struct DGCParam : public dmlc::Parameter<DGCParam> {
  float s_percent;
  float sample_rate;
  DMLC_DECLARE_PARAMETER(DGCParam){
    DMLC_DECLARE_FIELD(s_percent)
      .set_default(0.01)
      .describe("Range of values:(0,1], determines how many values should be sent out");
    DMLC_DECLARE_FIELD(sample_rate)
      .set_default(0.001)
      .describe("input.size * sample_rate = sample_cnt");
  };

};

inline bool DGCROpShape(const nnvm::NodeAttrs& attrs,
                             mxnet::ShapeVector* in_attrs,
                             mxnet::ShapeVector* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  const DGCRParam& param = nnvm::get<DGCRParam>(attrs.parsed);
  size_t output_size = param.original_size;
  if (out_attrs->at(0).ndim()==0U){
    out_attrs->at(0) = in_attrs->at(0); // copy ndim
    out_attrs->at(0)[0] = output_size;
  }
  else{
    CHECK_EQ(out_attrs->at(0)[0],output_size);
  };

  return true;
}

inline bool DGCOpShape(const nnvm::NodeAttrs& attrs,
                             mxnet::ShapeVector* in_attrs,
                             mxnet::ShapeVector* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  size_t output_size = in_attrs->at(0)[0]*2+2;
  if (out_attrs->at(0).ndim()==0U){
    out_attrs->at(0) = in_attrs->at(0); // copy ndim
    out_attrs->at(0)[0] = output_size;
  }
  else{
    CHECK_EQ(out_attrs->at(0)[0],output_size);
  };

  return true;
}

inline bool DGCROpType(const nnvm::NodeAttrs& attrs,
                            std::vector<int>* in_attrs,
                            std::vector<int>* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  out_attrs->at(0) = 0; //KFloat32=0, original data should be float 32 type.
  return in_attrs->at(0) == 0; //sparsity is set as float32 data.
}

inline bool DGCOpType(const nnvm::NodeAttrs& attrs,
                            std::vector<int>* in_attrs,
                            std::vector<int>* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U);
  CHECK_EQ(out_attrs->size(), 1U);

  out_attrs->at(0) = 0; //KFloat32=0, could transfer to int by (uint32_t)
  return in_attrs->at(0) == 0; //original data, should be float 32 type
}



//#define tblob_index(obj,type,index) ((type*)(obj.dptr<type>()))[index]
#define to_array(obj,to_type,from_type) ((to_type*)(obj.dptr<from_type>()))

inline bool cmp(float a, float b){
  return a>b;
}

template<typename xpu>
void DGCOpForward(const nnvm::NodeAttrs& attrs,
                        const OpContext& ctx,
                        const std::vector<TBlob>& inputs,
                        const std::vector<OpReqType>& req,
                        const std::vector<TBlob>& outputs) {
  static std::random_device r_seed;
  static std::default_random_engine e(r_seed());

  CHECK_EQ(inputs.size(), 1U);
  CHECK_EQ(outputs.size(), 1U);
  CHECK_EQ(req.size(), 1U);
  //mshadow::Stream<xpu> *s = ctx.get_stream<xpu>();
  const TBlob& in_data = inputs[0];
  const TBlob& out_data = outputs[0];
  size_t input_size = in_data.Size();
  //size_t output_size = out_data.Size();

  auto in_float = to_array(in_data,float,float);
  auto out_float = to_array(out_data,float,float);
  auto out_int32_t = to_array(out_data,int32_t,float);
  // auto out_int64_t = to_array(out_data,int64_t,float);

  using namespace mxnet_op;
  
  const DGCParam& param = nnvm::get<DGCParam>(attrs.parsed);
  size_t sample_cnt = std::max((size_t)(input_size*param.sample_rate),(size_t)(10/param.s_percent)); // i think 10 should be replaced with 100.
  //sample_cnt = std::min(sample_cnt, input_size);
  std::cout<<"sample_cnt="<<sample_cnt<<std::endl;
  if (sample_cnt <= input_size){
    std::uniform_int_distribution<uint32_t> uniform_dist(0,input_size-1); // random 
    for (uint32_t i = 0; i < sample_cnt; i++){
      auto index = uniform_dist(e);
      // out_float[i*2] = in_float[index];
      // out_int32_t[i*2+1] = index;
      out_float[i] = std::abs(in_float[index]);
    };
  }
  else{
    for (uint32_t i = 0; i < input_size; i++){
      // out_float[i*2] = in_float[i];
      // out_int32_t[i*2+1] = i;
      out_float[i] = std::abs(in_float[i]);
    };
    sample_cnt = input_size;
  };
  // std::sort(out_int64_t,out_int64_t+sample_cnt,cmp);
  std::sort(out_float,out_float+sample_cnt,cmp);

  size_t threshold_index = (size_t)(sample_cnt*param.s_percent);
  float threshold = out_float[threshold_index];
  size_t index = 0;
  for (uint32_t i = 0; i < input_size; i++){
    if (std::abs(in_float[i]) >= threshold){
      out_float[index*2] = in_float[i];
      out_int32_t[index*2+1] = i;
      in_float[i] = 0;
      index++;
    };
  };
  size_t paper_size = input_size*2+2;
  out_int32_t[index*2] = input_size;
  out_int32_t[paper_size-1] = index;
}

template<typename xpu>
void DGCROpForward(const nnvm::NodeAttrs& attrs,
                        const OpContext& ctx,
                        const std::vector<TBlob>& inputs,
                        const std::vector<OpReqType>& req,
                        const std::vector<TBlob>& outputs) {
  using namespace mxnet_op;
  // const DGCRParam& param = nnvm::get<DGCRParam>(attrs.parsed);

  CHECK_EQ(inputs.size(), 1U);
  CHECK_EQ(outputs.size(), 1U);
  CHECK_EQ(req.size(), 1U);
  //mshadow::Stream<xpu> *s = ctx.get_stream<xpu>();
  const TBlob& in_data = inputs[0];
  const TBlob& out_data = outputs[0];
  size_t input_size = in_data.Size();
  size_t output_size = out_data.Size();

  auto in_float = to_array(in_data,float,float);
  auto in_int32_t = to_array(in_data,int32_t,float);
  auto out_float = to_array(out_data,float,float);

  for (size_t i = 0; i < output_size; i++){
    out_float[i] = 0;
  };
  for (size_t i = 0; i < input_size/2; i++){
    float value = in_float[i*2];
    int32_t index = in_int32_t[i*2+1];
    out_float[index] = value;
  };
}

}  // namespace op
}  // namespace mxnet

#endif  // MXNET_OPERATOR_CONTRIB_DGC_OP_INL_H_
