#include "./generate_random-inl.h"
#include <thrust/execution_policy.h>


namespace mxnet {
namespace op {

NNVM_REGISTER_OP(_contrib_float_random)
.set_attr<FCompute>("FCompute<gpu>", floatRandomImpl<gpu,thrust::cuda_cub::par_t::stream_attachment_type>)
;

}
}