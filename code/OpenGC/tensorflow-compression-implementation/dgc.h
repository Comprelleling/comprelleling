#ifndef _TENSORFLOW_DGC_H
#define _TENSORFLOW_DGC_H

// #define ZQDEBUG 1


#include "absl/memory/memory.h"
#include "absl/strings/str_cat.h"
#include "absl/strings/string_view.h"
#include "tensorflow/core/framework/function.h"
#include "tensorflow/core/framework/graph_to_functiondef.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/lib/core/refcount.h"
#include "tensorflow/core/lib/strings/str_util.h"
#include "tensorflow/core/lib/strings/strcat.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/platform/mutex.h"
#include "tensorflow/core/platform/stream_executor.h"
#include "tensorflow/core/platform/thread_annotations.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/framework/shape_inference.h"
// #include "/home/togo/tensorflow/bazel-bin/external/local_config_cuda/cuda/_virtual_includes/cuda_headers_virtual/third_party/gpus/cuda/include/driver_types.h"
#include "bazel-bin/external/local_config_cuda/cuda/_virtual_includes/cuda_headers_virtual/third_party/gpus/cuda/include/driver_types.h"

#include <type_traits>
#include "OpenGC/get_stream/get_stream_tensorflow.h"

#include "OpenGC/operate_memory/get_policy_general.h"
#include "OpenGC/gradient_compression_body/dgc_body.h"
#include "OpenGC/likely.h"
using namespace zq_cpp_lib::operate_memory;
using namespace zq_cpp_lib::gradient_compression_body;

using namespace tensorflow;
typedef Eigen::ThreadPoolDevice CPUDevice;
typedef Eigen::GpuDevice GPUDevice;

#include <math.h>

template <typename Device, typename policy_t>
class DgcOp: public OpKernel{
 public:
    explicit DgcOp(OpKernelConstruction* context): OpKernel(context){
        OP_REQUIRES_OK(context, context->GetAttr("start", &start));
        OP_REQUIRES_OK(context, context->GetAttr("len", &len));
        OP_REQUIRES_OK(context, context->GetAttr("s_percent", &s_percent));
        OP_REQUIRES_OK(context, context->GetAttr("sample_rate", &sample_rate));
        OP_REQUIRES_OK(context, context->GetAttr("bypass", &bypass));
    }
    void Compute(OpKernelContext* context) override{
        #ifdef ZQDEBUG
        if (std::is_same<Device, CPUDevice>::value){
            printf("%s[LINE:%d, Time:%s]: Hello from CPU\n", __FILE__, __LINE__, __TIME__);
        }
        else{
            printf("%s[LINE:%d, Time:%s]: Hello from GPU\n", __FILE__, __LINE__, __TIME__);
        }
        #endif
        int rate=100000;
        double _s_percent = static_cast<double>(trunc(s_percent*rate))/rate;
        double _sample_rate = static_cast<double>(trunc(sample_rate*rate))/rate;
        auto stream = get_stream<policy_t>::get(context);
        policy_t policy = get_policy<policy_t>::get(stream);
        bool mutable_flag=true;
        Tensor G_tensor = context->mutable_input(0,mutable_flag);
        auto G = G_tensor.flat<float>();
        auto G_size = len==-1? G.size():len;
        auto auxiliary_size = G_size * 4;
        TensorShape auxiliary_shape;
        auxiliary_shape.AddDim(auxiliary_size);
        Tensor auxiliary_tensor;
        #ifdef ZQDEBUG
        printf("to call allocate_temp\n");
        #endif
        context->allocate_temp(tensorflow::DT_UINT8, auxiliary_shape, &auxiliary_tensor);
        auto auxiliary = auxiliary_tensor.flat<uint8_t>();
        #ifdef ZQDEBUG
        printf("to call allocate_output\n");
        #endif

        auto Q_size = (ceil(G_size * _s_percent) * 2 ) * 4 + 4;
        TensorShape Q_shape;
        Q_shape.AddDim(Q_size);
        Tensor* Q_tensor = NULL;
        context->allocate_output(0, Q_shape, &Q_tensor);
        auto Q = Q_tensor->flat<uint8_t>();

        #ifdef ZQDEBUG
            printf("\033[35m%s[LINE:%d, Time:%s]: name=", __FILE__, __LINE__, __TIME__);
            std::cout<<name()<<"\toutput_size="<<Q_size<<"\tinput_size="<<G_size;
            printf("\tstart=%d\tlen=%d",start,len);
            printf("\033[37m\n");
        #endif
        if (bypass){
            return;
        }
        int ret = dgc_with_auxiliary_body<policy_t>(
            G.data()+start,
            G_size,
            auxiliary.data(),
            Q.data(),
            _sample_rate,
            _s_percent,
            policy,
            stream
        );
        if (unlikely(ret!=0)){
            printf("dgc failed! Error Code = %d\n", ret);
            DCHECK(0);
        }
    }
 private:
    int start;
    int len;
    float s_percent;
    float sample_rate;
    int bypass;
};

template <typename Device, typename policy_t>
class DgcROp: public OpKernel{
 public:
    explicit DgcROp(OpKernelConstruction* context):OpKernel(context){
        OP_REQUIRES_OK(context, context->GetAttr("is_add_to", &is_add_to));
        OP_REQUIRES_OK(context, context->GetAttr("offset", &offset));
        OP_REQUIRES_OK(context, context->GetAttr("bypass", &bypass));
    }
    void Compute(OpKernelContext* context)override{
        #ifdef ZQDEBUG
        if (std::is_same<Device, CPUDevice>::value){
            printf("%s[LINE:%d, Time:%s]: Hello from CPU\n", __FILE__, __LINE__, __TIME__);
        }
        else{
            printf("%s[LINE:%d, Time:%s]: Hello from GPU\n", __FILE__, __LINE__, __TIME__);
        }
        #endif
        auto stream = get_stream<policy_t>::get(context);
        policy_t policy = get_policy<policy_t>::get(stream);
        bool mutable_flag = true;
        Tensor Q_tensor = context->mutable_input(0,mutable_flag);
        Tensor G_tensor = context->mutable_input(1,mutable_flag);
        auto Q = Q_tensor.flat<uint8_t>();
        auto Q_size = Q.size();
        auto G = G_tensor.flat<float>();
        auto G_size = G.size();
        context->set_output(0, G_tensor);
        if (bypass){
            return;
        }
        int ret = dgc_r_body(
            Q.data()+offset,
            Q_size-offset,
            G.data(),
            G_size,
            is_add_to,
            policy,
            stream
        );
        if (unlikely(ret!=0)){
            printf("dgc_r failed! Error Code = %d\n", ret);
            DCHECK(0);
        }
    }
 private:
    int is_add_to;
    int offset;
    int bypass;
};

#endif