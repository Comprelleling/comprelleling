#include "ZQ_CPP_LIB/high_perf_algo/high_perf_algo.h"
#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include <algorithm>
#include <random>
#include <assert.h>
using zq_cpp_lib::high_perf_algo::abs;
using zq_cpp_lib::high_perf_algo::max;
int main(int argc, char** argv){
    printf("test abs...\n");
    int32_t a = -32;
    std::cout<<abs<int32_t>(a)<<std::endl;
    int64_t b = -64;
    std::cout<<abs<int64_t>(b)<<std::endl;
    float c = -32.2;
    std::cout<<abs<float>(c)<<std::endl;
    double d = -64.4;
    std::cout<<abs<double>(d)<<std::endl;
    
    float e = 32.2;
    std::cout<<abs<float>(e)<<std::endl;

    printf("test max...\n");
    std::random_device rd;
    std::mt19937 gen(rd());
    int32_t lowerbound = 1<<31;
    printf("lowerbound = %d\n", lowerbound);
    int32_t upperbound = ~(1<<31);
    printf("upperbound = %d\n", upperbound);
    std::uniform_int_distribution<int32_t> dis(lowerbound, upperbound);
    {
    int32_t a = 984788221;
    int32_t b = -1285639474;
    int32_t c = max<int32_t>(a,b);
    printf("c=%d\n",c);
    }
    return 0;
    for (int i = 0; i < 1000; i++){
        auto a = dis(gen);
        auto b = dis(gen);
        auto c = max<int32_t>(a,b);
        auto d = std::max(a,b);
        if (c!=d){
            std::cout<<"Error: "<<a<<' '<<b<<' '<<c<<' '<<d<<std::endl;
            return -1;
        }
        if (false)
            std::cout<<"max("<<a<<","<<b<<")="<<c<<std::endl;
    }
    printf("max test pass!\n");
    return 0;
}
