#ifndef MXNET_OPERATOR_CONTRIB_ecq_sgd_INL_H
#define MXNET_OPERATOR_CONTRIB_ecq_sgd_INL_H

#include <mxnet/operator_util.h>
#include <vector>
#include "../mshadow_op.h"
#include "../mxnet_op.h"
#include "../operator_common.h"
#include "../elemwise_op_common.h"
#include "../tensor/init_op.h"

#include "ZQ_CPP_LIB/operate_memory/get_policy_general.h"
#include "ZQ_CPP_LIB/gradient_compression_body/ecq_body.h"
#include "get_stream_mxnet.h"
#include "ZQ_CPP_LIB/naive_random.hpp"
#include "ZQ_CPP_LIB/time_cost.hpp"
#include "ZQ_CPP_LIB/likely.h"



#define to_array(obj,to_type,from_type) ((to_type*)(obj.dptr<from_type>()))

namespace mxnet {
namespace op {
using namespace zq_cpp_lib::operate_memory;
using namespace zq_cpp_lib::gradient_compression_body;

struct ecq_sgd_param : public dmlc::Parameter<ecq_sgd_param> {
  double alpha;
  double beta;
  int32_t bitwidth;
  DMLC_DECLARE_PARAMETER(ecq_sgd_param){
    DMLC_DECLARE_FIELD(alpha)
      .set_default(1.0)
      .describe("range: (0,?), meaning: influence factor of residual")
      ;
    DMLC_DECLARE_FIELD(beta)
      .set_default(0.8)
      .describe("range: (0,1], meaning: decay factor of residual.")
      ;
    DMLC_DECLARE_FIELD(bitwidth)
      .set_default(2)
      .describe("range: {2,4,8}, meaning: bit width used to express a quantized data.")
      ;
  };
};


inline bool ecq_sgd_shape(const nnvm::NodeAttrs& attrs,
                             mxnet::ShapeVector* in_attrs,
                             mxnet::ShapeVector* out_attrs) {
  CHECK_EQ(out_attrs->at(0).ndim(), 1U)
    << "Please provide an output vector with ndim = 1";
  auto g_size = in_attrs->at(0)[0];
  auto h_size = in_attrs->at(1)[0];
  auto q_size = out_attrs->at(0)[0];
  CHECK_EQ(g_size, h_size)
    << "Gradients(g) size should be equal to residual(h) size";
  const ecq_sgd_param& param = nnvm::get<ecq_sgd_param>(attrs.parsed);
  uint8_t data_per_byte = 8/param.bitwidth;
  // int32_t min_q_size = static_cast<int32_t>(ceil(g_size*1.0/data_per_byte)) + g_size*4;
  int32_t q_size_1 = static_cast<int32_t>(ceil(g_size*1.0/data_per_byte))+10;
  int32_t q_size_2 = g_size * 4;
  int32_t min_q_size = q_size_1 > q_size_2 ? q_size_1 : q_size_2;
  
  CHECK_GE(q_size, min_q_size)
    << "Output(q) size should >= "
    << min_q_size 
    << " for input size = "
    << g_size
    ;
  return true;
}

inline bool ecq_sgd_type(const nnvm::NodeAttrs& attrs,
                            std::vector<int>* in_attrs,
                            std::vector<int>* out_attrs) {
  CHECK_EQ(in_attrs->size(), 2U) << "Input: data(g), residual(h).";
  CHECK_EQ(out_attrs->size(), 1U) << "Please provide output space";
  CHECK_EQ(in_attrs->at(0), 0) << "data(g) type should be float32";
  CHECK_EQ(in_attrs->at(1), 0) << "residual(h) type should be float32";
  CHECK_EQ(out_attrs->at(0),3) << "output(q) type should be uint8";
  return true;
}

template<typename xpu, typename policy_t>
void ecq_sgd_func(const nnvm::NodeAttrs& attrs,
                        const OpContext& ctx,
                        const std::vector<TBlob>& inputs,
                        const std::vector<OpReqType>& req,
                        const std::vector<TBlob>& outputs) {
  void* stream = get_stream<policy_t>::get(ctx);
  policy_t policy = get_policy<policy_t>::get(stream);

  const ecq_sgd_param& param = nnvm::get<ecq_sgd_param>(attrs.parsed);
  const TBlob& in_data = inputs[0];
  const TBlob& residual_data = inputs[1];
  const TBlob& out_data = outputs[0];
  uint8_t* out_uint8_t = to_array(out_data, uint8_t, uint8_t);
  float* G = to_array(in_data, float, float);
  float* H = to_array(residual_data, float, float);
  int ret = ecq_body<policy_t>(
    G,
    in_data.Size(),
    H,
    residual_data.Size(),
    out_uint8_t,
    out_data.Size(),
    param.alpha,
    param.beta,
    param.bitwidth,
    policy,
    stream
  );
  if (unlikely(ret!=0)){
    printf("ecq_sgd_func failed! Error Code = %d\n", ret);
    CHECK_EQ(0,1);
  }
}


struct ecq_sgd_r_param : public dmlc::Parameter<ecq_sgd_r_param> {
  int32_t is_add_to;
  DMLC_DECLARE_PARAMETER(ecq_sgd_r_param){
    DMLC_DECLARE_FIELD(is_add_to)
      .set_default(0)
      .describe("0: write to; 1: add to")
      ;
  };
};

inline bool ecq_sgd_r_shape(const nnvm::NodeAttrs& attrs,
                             mxnet::ShapeVector* in_attrs,
                             mxnet::ShapeVector* out_attrs) {
  CHECK_EQ(out_attrs->at(0).ndim(), 1U)
    <<"Please provide an output vector with ndim = 1";
  return true;
}

inline bool ecq_sgd_r_type(const nnvm::NodeAttrs& attrs,
                            std::vector<int>* in_attrs,
                            std::vector<int>* out_attrs) {
  CHECK_EQ(in_attrs->at(0),3)
    << "input tyep should be uint8";
  CHECK_EQ(out_attrs->at(0),0)
    << "output type should be float32";
  return true;
}


template<typename xpu, typename policy_t>
void ecq_sgd_r_func(const nnvm::NodeAttrs& attrs,
                        const OpContext& ctx,
                        const std::vector<TBlob>& inputs,
                        const std::vector<OpReqType>& req,
                        const std::vector<TBlob>& outputs) {
  void *stream = get_stream<policy_t>::get(ctx);
  policy_t policy = get_policy<policy_t>::get(stream);
  const ecq_sgd_r_param& param = nnvm::get<ecq_sgd_r_param>(attrs.parsed);
  const TBlob& in_data = inputs[0];
  const TBlob& out_data = outputs[0];
  auto in_uint8_t = to_array(in_data, uint8_t, uint8_t);
  auto out_float = to_array(out_data, float, float);
  int ret = ecq_r_body<policy_t>(
    in_uint8_t,
    in_data.Size(),
    out_float,
    out_data.Size(),
    param.is_add_to,
    policy,
    stream
  );
  if (unlikely(ret!=0)){
    printf("ecq_sgd_r_func failed! Error Code = %d\n",ret);
    CHECK_EQ(0,1);
  }
}

}
}
#endif