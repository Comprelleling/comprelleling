#include "../naive_random.hpp"
#include <iomanip>
#include <iostream>
using namespace std;

int main(){
    zq_cpp_lib::naive_int_random<uint16_t> q(0,128);
    q.srand(time(NULL));
    for (int i = 0; i < 100; i++){
        cout<<q()<<'\t';
    }
    cout<<endl;
    zq_cpp_lib::naive_real_random<float> r;
    r.srand(time(NULL)+1);
    for (int i =0; i< 100; i++){
        cout<<setprecision(2) << r()<<'\t';
    }
    cout<<endl;
    return 0;
}