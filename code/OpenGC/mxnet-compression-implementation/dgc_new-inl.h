#ifndef MXNET_OPERATOR_CONTRIB_dgc_new_INL_H
#define MXNET_OPERATOR_CONTRIB_dgc_new_INL_H

#include <mxnet/operator_util.h>
#include <vector>
#include "../mshadow_op.h"
#include "../mxnet_op.h"
#include "../operator_common.h"
#include "../elemwise_op_common.h"
#include "../tensor/init_op.h"

#include "ZQ_CPP_LIB/operate_memory/get_policy_general.h"
#include "ZQ_CPP_LIB/gradient_compression_body/dgc_body.h"
#include "get_stream_mxnet.h"
#include "ZQ_CPP_LIB/naive_random.hpp"
#include "ZQ_CPP_LIB/time_cost.hpp"
#include "ZQ_CPP_LIB/likely.h"

#define to_array(obj,to_type,from_type) ((to_type*)(obj.dptr<from_type>()))

namespace mxnet {
namespace op {
using namespace zq_cpp_lib::operate_memory;
using namespace zq_cpp_lib::gradient_compression_body;


struct dgc_new_param : public dmlc::Parameter<dgc_new_param> {
  double s_percent;
  double sample_rate;
  DMLC_DECLARE_PARAMETER(dgc_new_param){
    DMLC_DECLARE_FIELD(s_percent)
      .set_default(0.01)
      .describe("Range of values:(0.001,0.1], determines how many values should be sent out");
    DMLC_DECLARE_FIELD(sample_rate)
      .set_default(0.001)
      .describe("input.size * sample_rate = sample_cnt");
  };
};

inline bool dgc_new_shape(const nnvm::NodeAttrs& attrs,
                             mxnet::ShapeVector* in_attrs,
                             mxnet::ShapeVector* out_attrs) {
  CHECK_EQ(out_attrs->at(0).ndim(), 1U)
    << "Please provide an output vector with ndim = 1";
  // const dgc_new_param& param = nnvm::get<dgc_new_param>(attrs.parsed);
  auto in_size = in_attrs->at(0)[0];
  auto out_size = out_attrs->at(0)[0];
  auto min_out_size = in_size * 4;
  CHECK_GE(out_size, min_out_size)
   << "Out size should >= " << min_out_size << "for input size = " << in_size;
  return true;
}

inline bool dgc_new_type(const nnvm::NodeAttrs& attrs,
                            std::vector<int>* in_attrs,
                            std::vector<int>* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U) << "Input: data";
  CHECK_EQ(out_attrs->size(), 1U) << "Please provide output space";
  CHECK_EQ(in_attrs->at(0) ,0) << "data type should be float32";
  CHECK_EQ(out_attrs->at(0),3) <<"output type should be uint8_t";
  return true;
}

template<typename xpu, typename policy_t>
void dgc_new_func(const nnvm::NodeAttrs& attrs,
                        const OpContext& ctx,
                        const std::vector<TBlob>& inputs,
                        const std::vector<OpReqType>& req,
                        const std::vector<TBlob>& outputs) {
  void* stream = get_stream<policy_t>::get(ctx);
  auto policy = get_policy<policy_t>::get(stream);
  const dgc_new_param& param = nnvm::get<dgc_new_param>(attrs.parsed);
  const TBlob& in_data = inputs[0];
  const TBlob& out_data = outputs[0];
  float* in_float = to_array(in_data, float, float);
  uint8_t* out_uint8_t = to_array(out_data, uint8_t, uint8_t);
  int32_t N = in_data.Size();
  int ret = dgc_body<policy_t>(
    in_float,
    N,
    out_uint8_t,
    param.sample_rate,
    param.s_percent,
    policy,
    stream
  );
  if (unlikely(ret!=0)){
    printf("dgc_new_func failed! Error code = %d\n", ret);
    CHECK_EQ(0,1);
  }
}


struct dgc_new_r_param : public dmlc::Parameter<dgc_new_r_param> {
  int is_add_to;
  DMLC_DECLARE_PARAMETER(dgc_new_r_param){
    DMLC_DECLARE_FIELD(is_add_to)
      .set_default(1)
      .describe("1: add_to; 0:write_to")
      ;
  };
};

inline bool dgc_new_r_shape(const nnvm::NodeAttrs& attrs,
                             mxnet::ShapeVector* in_attrs,
                             mxnet::ShapeVector* out_attrs) {
  CHECK_EQ(out_attrs->at(0).ndim(),1U)
    << "Please provide an output vector with ndim = 1";
  return true;
}

inline bool dgc_new_r_type(const nnvm::NodeAttrs& attrs,
                            std::vector<int>* in_attrs,
                            std::vector<int>* out_attrs) {
  CHECK_EQ(in_attrs->size(), 1U) << "Input: data";
  CHECK_EQ(out_attrs->size(), 1U) << "Please provide output space";
  CHECK_EQ(in_attrs->at(0), 3) << "data type should be uint8_t";
  CHECK_EQ(out_attrs->at(0), 0) << "output type should be float32";
  return true;
}


template<typename xpu, typename policy_t>
void dgc_new_r_func(const nnvm::NodeAttrs& attrs,
                        const OpContext& ctx,
                        const std::vector<TBlob>& inputs,
                        const std::vector<OpReqType>& req,
                        const std::vector<TBlob>& outputs) {
  void* stream = get_stream<policy_t>::get(ctx);
  policy_t policy = get_policy<policy_t>::get(stream);
  const dgc_new_r_param& param = nnvm::get<dgc_new_r_param>(attrs.parsed);
  const TBlob& in_data = inputs[0];
  const TBlob& out_data = outputs[0];
  auto in_uint8_t = to_array(in_data, uint8_t, uint8_t);
  auto out_float = to_array(out_data, float, float);
  int ret = dgc_r_body<policy_t>(
    in_uint8_t,
    in_data.Size(),
    out_float,
    out_data.Size(),
    param.is_add_to,
    policy,
    stream
  );
  if (unlikely(ret!=0)){
    printf("dgc_new_r_func failed! Error code=%d\n", ret);
    CHECK_EQ(0,1);
  }
}

}
}
#endif