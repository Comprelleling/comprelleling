#include "./tbq-inl.h"
#include <thrust/execution_policy.h>  //thrust::device
#include <thrust/system/omp/execution_policy.h>

namespace mxnet{
namespace op{
  DMLC_REGISTER_PARAMETER(tbqParam);
  DMLC_REGISTER_PARAMETER(tbqRParam);

  NNVM_REGISTER_OP(_contrib_zgc)
    .set_attr_parser(ParamParser<tbqParam>)
    .set_num_inputs(2)
    .set_num_outputs(1)
    .set_attr<nnvm::FListInputNames>("FListInputNames",
      [](const NodeAttrs& attrs){
        return std::vector<std::string>{"data", "residual"};
      }
    )
    .set_attr<mxnet::FInferShape>("FInferShape", tbqShape)
    .set_attr<nnvm::FInferType>("FInferType", tbqType)
    //.set_attr<nnvm::FInplaceOption>
    .add_argument("data", "NDArray-or-Symbol","data")
    .add_argument("residual", "NDArray-or-Symbol", "residual")
    .add_arguments(tbqParam::__FIELDS__())
    ;

  NNVM_REGISTER_OP(_contrib_zgcr)
    .set_attr_parser(ParamParser<tbqRParam>)
    .set_num_inputs(1)
    .set_num_outputs(1)
    .set_attr<nnvm::FListInputNames>("FListInputNames",
      [](const NodeAttrs& attrs){
        return std::vector<std::string>{"data"};
      }
    )
    .set_attr<mxnet::FInferShape>("FInferShape", tbqRShape)
    .set_attr<nnvm::FInferType>("FInferType", tbqRType)
    //.set_attr<nnvm::FInplaceOption>
    .add_argument("data", "NDArray-or-Symbol","data")
    .add_arguments(tbqRParam::__FIELDS__())
    ;
  
  NNVM_REGISTER_OP(_contrib_zgc)
    .set_attr<FCompute>("FCompute<cpu>", tbqImpl<cpu, thrust::detail::host_t>)
    ;
  NNVM_REGISTER_OP(_contrib_zgcr)
    .set_attr<FCompute>("FCompute<cpu>", tbqRImpl<cpu, thrust::detail::host_t>)
    ;

  
  NNVM_REGISTER_OP(_contrib_zgc_omp)
    .set_attr_parser(ParamParser<tbqParam>)
    .set_num_inputs(2)
    .set_num_outputs(1)
    .set_attr<nnvm::FListInputNames>("FListInputNames",
      [](const NodeAttrs& attrs){
        return std::vector<std::string>{"data", "residual"};
      }
    )
    .set_attr<mxnet::FInferShape>("FInferShape", tbqShape)
    .set_attr<nnvm::FInferType>("FInferType", tbqType)
    //.set_attr<nnvm::FInplaceOption>
    .add_argument("data", "NDArray-or-Symbol","data")
    .add_argument("residual", "NDArray-or-Symbol", "residual")
    .add_arguments(tbqParam::__FIELDS__())
    ;

  NNVM_REGISTER_OP(_contrib_zgcr_omp)
    .set_attr_parser(ParamParser<tbqRParam>)
    .set_num_inputs(1)
    .set_num_outputs(1)
    .set_attr<nnvm::FListInputNames>("FListInputNames",
      [](const NodeAttrs& attrs){
        return std::vector<std::string>{"data"};
      }
    )
    .set_attr<mxnet::FInferShape>("FInferShape", tbqRShape)
    .set_attr<nnvm::FInferType>("FInferType", tbqRType)
    //.set_attr<nnvm::FInplaceOption>
    .add_argument("data", "NDArray-or-Symbol","data")
    .add_arguments(tbqRParam::__FIELDS__())
    ;
  
  NNVM_REGISTER_OP(_contrib_zgc_omp)
    .set_attr<FCompute>("FCompute<cpu>", tbqImpl<cpu, thrust::system::omp::detail::par_t>)
    ;
  NNVM_REGISTER_OP(_contrib_zgcr_omp)
    .set_attr<FCompute>("FCompute<cpu>", tbqRImpl<cpu, thrust::system::omp::detail::par_t>)
    ;
}

}