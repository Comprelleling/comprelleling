#ifndef CONTRIB_tbq
#define CONTRIB_tbq

#include <dmlc/parameter.h>
#include <mshadow/base.h>
#include <mxnet/op_attr_types.h>
#include <mxnet/operator.h>
#include <mxnet/operator_util.h>
#include <nnvm/op.h>
#include <nnvm/op_attr_types.h>
#include <vector>
#include "../elemwise_op_common.h"
#include "../mshadow_op.h"
#include "../mxnet_op.h"
#include "../operator_common.h"
#include "../tensor/init_op.h"
#include "../tensor/util/tensor_util-inl.h"

#include <thrust/execution_policy.h>  //thrust::device
#include <thrust/functional.h>  //greater<float>
#include <thrust/iterator/counting_iterator.h>  // counting_iterator
#include <thrust/transform.h> //trnasform

#include "get_stream_mxnet.h"
#include "ZQ_CPP_LIB/operate_memory/get_policy_general.h"
#include "ZQ_CPP_LIB/gradient_compression_body/tbq_body.h"
#include "ZQ_CPP_LIB/likely.h"


namespace mxnet{
namespace op{
  using namespace zq_cpp_lib::operate_memory;
  using namespace zq_cpp_lib::gradient_compression_body;
  struct tbqRParam: public dmlc::Parameter<tbqRParam>{
    float threshold;
    int is_add_to;
    DMLC_DECLARE_PARAMETER(tbqRParam){
      DMLC_DECLARE_FIELD(threshold)
        .set_default(1.0f)
        .describe("Greater than 0.");
      DMLC_DECLARE_FIELD(is_add_to)
        .set_default(0)
        .describe("0: write to; others: add to");
    }
  };
  struct tbqParam: public dmlc::Parameter<tbqParam>{
    float threshold;

    DMLC_DECLARE_PARAMETER(tbqParam){
      DMLC_DECLARE_FIELD(threshold)
        .set_default(1.0f)
        .describe("Greater than 0.");
    }
  };
  inline bool tbqRType(
    const nnvm::NodeAttrs &attrs,
    std::vector<int>* in_attrs,
    std::vector<int>* out_attrs
  ){
    CHECK_EQ(in_attrs->size(),1U) << "Input: data";
    CHECK_EQ(out_attrs->size(),1U);
    CHECK_EQ(in_attrs->at(0),3) 
      << "Only support decompression data with type == uint8.";
    CHECK_EQ(out_attrs->at(0),0) 
      << "Output data type should be float32.";
    return true;
  };
  inline bool tbqType(
    const nnvm::NodeAttrs &attrs,
    std::vector<int>* in_attrs,
    std::vector<int>* out_attrs
  ){
    CHECK_EQ(in_attrs->size(),2U) << "Input: data, residual.";
    CHECK_EQ(out_attrs->size(),1U);
    CHECK_EQ(in_attrs->at(0),0) 
      << "Only support compressiong float32 data.";
    CHECK_EQ(in_attrs->at(1),0) 
      << "Residual data type should be same type(float32) with data data.";
    CHECK_EQ(out_attrs->at(0),3) 
      << "Output data type should be uint8.";
    return true;
  };

  inline bool tbqRShape(
    const nnvm::NodeAttrs& attrs,
    mxnet::ShapeVector* in_attrs,
    mxnet::ShapeVector* out_attrs
  ){
    auto to_decompress_size = in_attrs->at(0)[0];
    auto original_size = out_attrs->at(0)[0];
    auto min_compressed_size = (original_size + 4 - 1 ) >> 2;
    CHECK_GE(to_decompress_size, min_compressed_size)
      << "to decompressed data size should be greater than or equal to ceil(out_data/16).";
    return true;
  };
  inline bool tbqShape(
    const nnvm::NodeAttrs& attrs,
    mxnet::ShapeVector* in_attrs,
    mxnet::ShapeVector* out_attrs
  ){
    CHECK_EQ(out_attrs->at(0).ndim(),1U) 
      << "please provide an output vector with ndim == 1";
    auto to_compress_size = in_attrs->at(0)[0];
    auto residual_size = in_attrs->at(1)[0];
    auto out_size = out_attrs->at(0)[0];
    CHECK_EQ(to_compress_size, residual_size)
      << "data size should equal to residual size.";
    auto min_out_size = (to_compress_size + 16 - 1 ) >> 4;
    CHECK_GE(out_size, min_out_size)
      << "out size should be greater than or equal to ceil(to_compress_size/16).";
    return true;
  };
  #define to_array(obj,to_type,from_type) ((to_type*)(obj.dptr<from_type>()))

  template<typename xpu, typename policy_t>
  void tbqImpl(
    const nnvm::NodeAttrs& attrs,
    const OpContext& ctx,
    const std::vector<TBlob>&inputs,
    const std::vector<OpReqType>& req,
    const std::vector<TBlob>& outputs
  ){
    void* stream = get_stream<policy_t>::get(ctx);
    policy_t policy = get_policy<policy_t>::get(stream);
    const tbqParam& param
      = nnvm::get<tbqParam>(attrs.parsed);
    const TBlob& data = inputs[0];
    const TBlob& residual = inputs[1];
    const TBlob& out_data = outputs[0];
    float* to_compress_float = to_array(data,float,float);
    float* residual_float = to_array(residual, float, float);
    uint8_t* out_uint8_t = to_array(out_data,uint8_t, uint8_t);
    int ret = tbq_body(
      to_compress_float,
      data.Size(),
      residual_float,
      residual.Size(),
      out_uint8_t,
      out_data.Size(),
      param.threshold,
      policy,
      stream
    );
    if (unlikely(ret!=0)){
      printf("tbqImpl failed! Error Code = %d\n", ret);
      CHECK_EQ(0,1);
    }
  }
  
  template<typename xpu, typename policy_t>
  void tbqRImpl(
    const nnvm::NodeAttrs& attrs,
    const OpContext& ctx,
    const std::vector<TBlob>&inputs,
    const std::vector<OpReqType>& req,
    const std::vector<TBlob>& outputs
  ){
    void* stream = get_stream<policy_t>::get(ctx);
    policy_t policy = get_policy<policy_t>::get(stream);
    const tbqRParam& param
      = nnvm::get<tbqRParam>(attrs.parsed);
    const TBlob& in_data = inputs[0];
    const TBlob& out_data = outputs[0];
    float* out_float = to_array(out_data, float, float);
    uint8_t* in_uint8_t = to_array(in_data, uint8_t, uint8_t);
    int ret = tbq_r_body(
      out_float,
      out_data.Size(),
      in_uint8_t,
      in_data.Size(),
      param.threshold,
      param.is_add_to,
      policy,
      stream
    );
    if (unlikely(ret!=0)){
      printf("ZqGradientCompressioinRImpl failed! Error Code = %d\n", ret);
      CHECK_EQ(0,1);
    }
  }
}
}

#endif