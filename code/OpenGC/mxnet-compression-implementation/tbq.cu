#include "./tbq-inl.h"
#include <thrust/execution_policy.h>  //thrust::device

namespace mxnet{
namespace op{
    NNVM_REGISTER_OP(_contrib_zgc)
        .set_attr<FCompute>("FCompute<gpu>", tbqImpl<gpu, thrust::cuda_cub::par_t::stream_attachment_type>)
        ;
    NNVM_REGISTER_OP(_contrib_zgcr)
        .set_attr<FCompute>("FCompute<gpu>", tbqRImpl<gpu, thrust::cuda_cub::par_t::stream_attachment_type>)
        ;
}
}