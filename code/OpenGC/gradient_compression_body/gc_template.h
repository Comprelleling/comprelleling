#ifndef ZQ_CPP_LIB_GRADIENT_COMPRESSION_BODY_GC_TEMPLATE_BODY_H
#define ZQ_CPP_LIB_GRADIENT_COMPRESSION_BODY_GC_TEMPLATE_BODY_H
#include <stdint.h>
#include <thrust/copy.h>                       //copy_if
#include <thrust/execution_policy.h>           //thrust::device
#include <thrust/functional.h>                 //greater<float>
#include <thrust/iterator/counting_iterator.h> // counting_iterator
#include <thrust/random.h>
#include <thrust/sort.h>      //sort()
#include <thrust/transform.h> //trnasform
#include "../likely.h"
#include "../naive_random.hpp"
#include "../operate_memory/get_policy_general.h"

namespace zq_cpp_lib
{
namespace gradient_compression_body
{
using namespace zq_cpp_lib::operate_memory;

template <typename policy_t>
int gc_template_body(
    float *G,
    int32_t G_size,
    uint8_t *out_uint8_t,
    int32_t out_uint8_t_size,
    float param1,
    policy_t policy,
    void *stream)
{
  // do compression
  return 0;
}

template <typename policy_t>
int gc_template_r_body(
    uint8_t *in_uint8_t,
    int32_t in_uint8_t_size,
    float *out_float,
    int32_t out_float_size,
    int is_add_to,
    policy_t policy,
    void *stream)
{
  // do decompression
  return 0;
}

} // namespace gradient_compression_body
} // namespace zq_cpp_lib

#endif
