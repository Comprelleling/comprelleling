#training gluon
NCCL_DEBUG=INFO NCCL_TREE_THRESHOLD=0 NCCL_IB_DISABLE=1 NCCL_SOCKET_IFNAME=ens14f1 horovodrun -np $1 \
    -H $2 \
	python $HOME/comprelleling/code/CG-allreduce/horovod/examples/mxnet_imagenet_resnet50.py \
        --mode gluon \
        --batch-size $3 \
        --num-epochs 3 \
        --log-interval 20 \
        --num-examples $4 \
        --model $5 \
		--image-shape $6 \
        --use-rec \
        --rec-train=$7 \
        --rec-train-idx=$8 \
        --rec-val="$HOME/trainData/imagenet1k-val.rec" \
        --rec-val-idx="$HOME/trainData/imagenet1k-val.idx" \
        --comp-alg=$9 \
        --comp-threshold=${10}
