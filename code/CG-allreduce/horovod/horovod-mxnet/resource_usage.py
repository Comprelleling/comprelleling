import os
import re
import sys
import time
import socket
import argparse
import threading

#applications parameters
params = 	{
					'resnet50' : ['resnet50_v2', '3,224,224'],
					'resnet152' : ['resnet152_v2', '3,224,224'],
                    'resnet18' : ['resnet18_v2', '3,224,224'],
					'vgg16' : ['vgg16', '3,224,224'],
					'vgg19' : ['vgg19', '3,224,224'],
					'inceptionv3' : ['inceptionv3', '3,299,299'],
					'inceptionv4' : ['inceptionv4', '3,299,299'],
					'lenet' : ['lenet', '3,224,224'],
					'alexnet' : ['alexnet', '3,224,224']
					}

# at local hosts
batchsize = {'resnet50' : 32, 'inceptionv3' : 32, 'inceptionv4' : 32, 'vgg16' : 16, 'vgg19' : 16, 'lenet' : 64, 'inception-v5' : 16, 'alexnet' : 128}
# at aws M60
# batchsize = {'resnet50' : 64, 'inception-v4' : 16, 'inception-v3' : 32, 'vgg16' : 32, 'vgg19' : 16, 'lenet' : 64, 'inception-v5' : 16, 'lenet1' : 64, 'resnet152' : 16}
# batchsize = {'resnet50' : 32, 'inception-v4' : 8, 'inception-v3' : 16, 'vgg19' : 8, 'resnet152' : 8}

#parse the parameters
def parseArgs():
	parser = argparse.ArgumentParser(
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        	description='measure the resource usage')
	parser.add_argument('--numprocess', type = int, default = 4,
							     help = 'the parameters for mpi -np')
	parser.add_argument('--servers', type = str, default = "egpu3:1,egpu4:1,egpu5:1,egpu6:1",
							     help = 'assign servers onto machine')
	parser.add_argument('--numexamples', type = int, default = 51200,
									 help = 'number of examples for training')
	parser.add_argument('--strongscaling', action = 'store_true', help = 'if true, update batch size strongscaling, else weak')
	parser.add_argument('--extra', type = str, help = 'add some info by user')
	parser.add_argument('--usetools', action = 'store_true', help = 'if true, use network tools')
	parser.add_argument('--profiler', action = 'store_true', help = 'mxnet profile')
	parser.add_argument('--interface', type = str, default = 'tenGE', choices = ['tenGE', 'ib'],
								 	 help = 'the environment to execute')
	parser.add_argument('--model', type = str, default = 'resnet50', choices = ['lenet', 'resnet152', 'vgg19', 'inceptionv4', 'resnet50', 'alexnet'],
								 	 help = 'the training model')
	parser.add_argument('--bulk', type = int, default = 15,
							     help = 'bulk size of mxnet engine')
	parser.add_argument('--comp-threshold', type = int, default = sys.maxsize,
							     help = 'compression threshold, number of float data')
	parser.add_argument('--comp-alg', type = str, default = '',
							     help = 'compression algorithm name')
	parser.add_argument('--horovodrun', action = 'store_true', help = 'use the horovodrun rather than mpirun')
	parser.add_argument('--lenet', action = 'store_true', help = 'use the lenet as training model')
	return parser.parse_args()


class trainingApp:
	"""traing applications"""
	def __init__(self, app, parsedArgs, resultpath):
		self.app = app
		self.resultpath = resultpath
	def training(self, parsedArgs):
		if "inception" in self.app:
			train_rec = "/home/gpu/trainData/traindata-299.rec"
			train_idx = "/home/gpu/trainData/traindata-299.idx"
		else:
			train_rec = "/home/gpu/trainData/traindata.rec"
			train_idx = "/home/gpu/trainData/traindata.idx"
		cmd = '''./%(horo)s.sh %(numproc)d %(serverset)s %(bsize)d %(examples)d %(model)s %(shape)s %(rec)s %(idx)s %(alg)s %(thr)d 2>&1 | tee %(path)s/imagenet_ringtrain_%(time)s.log'''%(
			{
				'numproc': parsedArgs.numprocess,
				'serverset': parsedArgs.servers,
				'model': params[self.app][0],
				'bsize': batchsize[self.app],
				'examples': parsedArgs.numexamples,
				'path': self.resultpath,
				'time': str(time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime())),
				'shape': params[self.app][1],
				'horo': 'horovodrun' if parsedArgs.horovodrun else 'horovod',
				'rec' : train_rec,
				'idx' : train_idx,
				'blk' : parsedArgs.bulk,
				'alg' : parsedArgs.comp_alg,
				'thr' : parsedArgs.comp_threshold,
			})
		print(cmd)
		os.system("echo %s > %s/command.log"%(cmd, self.resultpath))
		os.system(cmd)

def initialize():
	# initialize the batch size, strongscaling or weak
	if parsedArgs.strongscaling:
		for app in batchsize:
			batchsize[app] //= len(parsedArgs.servers.split(','))

def endOfonce(resultpath, parsedArgs):
	host_name_list = [elem.split(':')[0] for elem in parsedArgs.servers.split(',')]
	hostset = list()
	for hostname in host_name_list:
		x = re.search('.*?(\d+).*?', hostname)
		hostset.append(x.group(1))
	for hostid in hostset:
		hostname = 'egpu' + str(hostid)
		cmd = r'''ssh %s "kill \$(ps -aux | grep mxnet_imagenet | awk '{print \$2}')"'''%(hostname)
		print(cmd)
		os.system(cmd)
	print("end of main", time.ctime(time.time()))

if __name__ == '__main__':
	parsedArgs = parseArgs() #parse the parameters
	initialize()
	applications = [parsedArgs.model]
	for app in applications:
		dirname = str(time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime()))
		if parsedArgs.extra:
			dirname = dirname + parsedArgs.extra
		basepath = './trainResult/%s/%dmachines-%s/%s'%(app, len(parsedArgs.servers.split(',')), parsedArgs.numexamples, dirname)
		os.system("mkdir -p %s"%(basepath))

		train = trainingApp(app, parsedArgs, basepath)

		#start training
		# train.training(parsedArgs)
		thr = threading.Thread(target = train.training, args = (parsedArgs, ))
		thr.setDaemon(True)
		thr.start()

		time.sleep(10)
		thr.join()
		#finish the network, cpu, gpu tools per machine
		endOfonce(basepath, parsedArgs)
