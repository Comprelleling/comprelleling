mpirun -np $1 \
    -H $2 \
	-x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH -x NCCL_IB_DISABLE=1 -x NCCL_SOCKET_IFNAME=ens14f1 -x NCCL_TREE_THRESHOLD=0 -x HOROVOD_LOG_LEVEL="fatal" \
	\
    --mca btl tcp,self --mca btl_tcp_if_include ens14f1 \
    -bind-to none -map-by slot \
	python $HOME/comprelleling/code/CG-allreduce/horovod/examples/mxnet_imagenet_resnet50.py \
        --mode gluon \
        --batch-size $3 \
        --num-epochs 3 \
        --log-interval 20 \
        --num-examples $4 \
        --model $5 \
		--image-shape $6 \
        --use-rec \
        --rec-train=$7 \
        --rec-train-idx=$8 \
        --rec-val="$HOME/trainData/imagenet1k-val.rec" \
        --rec-val-idx="$HOME/trainData/imagenet1k-val.idx" \
        --comp-alg=$9 \
        --comp-threshold=${10}