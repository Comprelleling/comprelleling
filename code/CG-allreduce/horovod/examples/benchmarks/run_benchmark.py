import os
import colorful as cf

def write_run_shell(
    HOROVOD_CPU_FUSION_THRESHOLD=67108864,
    HOROVOD_LOG_LEVEL='"trace"',
    whatrun='horovodrun',
    devices={'localhost':1},
    cmd='python ./scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py',
    model='resnet50',
    batch_size=64,
    variable_update='horovod',
    compress_threshold=262144,
    observe=False
):
    H = ['{}:{}'.format(key, devices[key]) for key in devices]
    H = ','.join(H)
    np = sum([v for v in devices.values()])
    content=r'''
HOROVOD_CPU_FUSION_THRESHOLD={HOROVOD_CPU_FUSION_THRESHOLD}
HOROVOD_LOG_LEVEL={HOROVOD_LOG_LEVEL}
{whatrun} -np {np} -H {devices} {cmd} --model {model} --batch_size {batch_size} --variable_update {variable_update} --threshold {compress_threshold}
'''.format(
    HOROVOD_CPU_FUSION_THRESHOLD=HOROVOD_CPU_FUSION_THRESHOLD,
    HOROVOD_LOG_LEVEL=HOROVOD_LOG_LEVEL,
    whatrun=whatrun,
    np=np,
    devices=H,
    cmd=cmd,
    model=model,
    batch_size=batch_size,
    variable_update=variable_update,
    compress_threshold=compress_threshold
)
    with open("run_shell.sh", "w") as f:
        f.write(content)
    if observe==True:
        os.system("bash ./run_shell.sh")
    else:
        with os.popen("bash ./run_shell.sh") as r:
            text=r.read()
            pattern='total images/sec: '
            p=text.find(pattern)
            q=text.find('\n',p)
            speed=float(text[p+len(pattern):q])
            print(cf.yellow(text))
            print(cf.red(speed))
        with open("result.csv", "a") as f:
            line='{HOROVOD_CPU_FUSION_THRESHOLD},{whatrun},{np},{model},{batch_size},{variable_update},{compress_threshold},{speed}'.format(
                HOROVOD_CPU_FUSION_THRESHOLD=HOROVOD_CPU_FUSION_THRESHOLD,
                whatrun=whatrun,
                np=np,
                model=model,
                batch_size=batch_size,
                variable_update=variable_update,
                compress_threshold=compress_threshold,
                speed=speed
            )
            f.write(line+'\n')

def write_copy_shell(target_ip):
    if target_ip == 'localhost':
        return
    content=r"""
cp ../../horovod/tensorflow/__init__.py /home/togo/anaconda3/lib/python3.6/site-packages/horovod/tensorflow/
cp ../../horovod/tensorflow/mpi_ops.py /home/togo/anaconda3/lib/python3.6/site-packages/horovod/tensorflow/
qcp -i {target_ip} /home/togo/anaconda3/usr/horovod/
qcp -i {target_ip} /home/togo/anaconda3/bin/horovodrun
qcp -i {target_ip} /home/togo/anaconda3/lib/python3.6/site-packages/horovod*
cp /home/togo/tensorflow/tensorflow/core/user_ops/terngrad*o ./
scp -r /home/togo/horovod/examples/benchmarks togo@{target_ip}:/home/togo/horovod/examples/
""".format(
    target_ip = target_ip
)
    with open("copy_shell.sh", "w") as f:
        f.write(content)
    os.system("bash ./copy_shell.sh")
        
def run_one_GPU_per_node(devices=['localhost','egpu2']):
    with open("result.csv","w") as f:
        line='HOROVOD_CPU_FUSION_THRESHOLD,whatrun,np,model,batch_size,variable_update,compress_threshold,speed'
        f.write(line+'\n')
    for device in devices:
        if device=='localhost':
            continue
        write_copy_shell(device)
    np=1<<10
    nps = []
    while True:
        np = np >> 1
        if np > len(devices):
            continue
        if np == 1:
            break
        nps.append(np)
    nps = [8]
    for np in nps:
        H = dict()
        for i in range(np):
            H[devices[i]] = 1
        print("H={}".format(H))
        # for compress_threshold in [1<<28, 262144, 0]:
        for compress_threshold in [262144, 1<<28]:
            # for model in ['resnet50', 'vgg19']:
            for model in ['vgg19']:
                if model=='resnet50':
                    batch_size=32
                elif model == 'vgg19':
                    batch_size= 16
                write_run_shell(
                    devices=H, 
                    model=model,
                    compress_threshold=compress_threshold,
                    batch_size=batch_size
                )
def run_test(devices=['localhost','egpu2']):
    np=2
    for device in devices:
        if device=='localhost':
            continue
        write_copy_shell(device)
    H = dict()
    for i in range(np):
        H[devices[i]] = 1
    compress_threshold=262144
    model='vgg19'
    batch_size=16
    write_run_shell(
        devices=H,
        model=model,
        compress_threshold=compress_threshold,
        batch_size=batch_size,
        observe=True
    )


if __name__ == '__main__':
    # write_copy_shell()
    run_one_GPU_per_node(devices=[
        'localhost','egpu2','egpu17','egpu18',
        'egpu13','egpu14','egpu15','egpu16'
    ])
    # run_test()
