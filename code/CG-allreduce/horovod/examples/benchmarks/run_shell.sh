
HOROVOD_CPU_FUSION_THRESHOLD=67108864
HOROVOD_LOG_LEVEL="trace"
horovodrun -np 2 -H localhost:1,egpu2:1 python ./scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py --model vgg19 --batch_size 16 --variable_update horovod --threshold 0
