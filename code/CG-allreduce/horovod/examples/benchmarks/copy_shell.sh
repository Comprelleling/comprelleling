
cp ../../horovod/tensorflow/__init__.py /home/togo/anaconda3/lib/python3.6/site-packages/horovod/tensorflow/
cp ../../horovod/tensorflow/mpi_ops.py /home/togo/anaconda3/lib/python3.6/site-packages/horovod/tensorflow/
qcp -i egpu2 /home/togo/anaconda3/usr/horovod/
qcp -i egpu2 /home/togo/anaconda3/bin/horovodrun
qcp -i egpu2 /home/togo/anaconda3/lib/python3.6/site-packages/horovod*
cp /home/togo/tensorflow/tensorflow/core/user_ops/terngrad*o ./
scp -r /home/togo/horovod/examples/benchmarks togo@egpu2:/home/togo/horovod/examples/
