# Copyright 2018 Uber Technologies, Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from horovod.common import check_extension

try:
    check_extension('horovod.torch', 'HOROVOD_WITH_PYTORCH',
                    __file__, 'mpi_lib_v2')
except:
    check_extension('horovod.torch', 'HOROVOD_WITH_PYTORCH',
                    __file__, 'mpi_lib', '_mpi_lib')

from horovod.torch.compression import Compression
from horovod.torch.mpi_ops import allreduce, allreduce_async, allreduce_, allreduce_async_
from horovod.torch.mpi_ops import allgather, allgather_async
from horovod.torch.mpi_ops import broadcast, broadcast_async, broadcast_, broadcast_async_
from horovod.torch.mpi_ops import gather_async_
from horovod.torch.mpi_ops import cbroadcast_async_
from horovod.torch.mpi_ops import poll, synchronize
from horovod.torch.mpi_ops import init, shutdown
from horovod.torch.mpi_ops import size, local_size, rank, local_rank
from horovod.torch.mpi_ops import mpi_threads_supported

import torch
import collections

from caffe2.python import core,workspace
import  caffe2.python.hypothesis_test_util as hu

class _DistributedOptimizer(torch.optim.Optimizer):
    def _terngrad_init(self):
        algorithm_name = 'terngrad'
        self._param[algorithm_name] = {
            'bitwidth':2,
            'random':0
        }
        self._terngrad = []
        self._terngrad_r = []
        self._terngrad_gpu_op = core.CreateOperator(
            'terngrad',
            ['original', 'compressed'],
            [],
            bitwidth = self._param[algorithm_name]['bitwidth'],
            random = 0,
            device_option = hu.cuda_do
        )
        self._terngrad.append(self._terngrad_gpu_op)
        self._terngrad_gpu_op = core.CreateOperator(
            'terngrad',
            ['original', 'compressed'],
            [],
            bitwidth = self._param[algorithm_name]['bitwidth'],
            random = 1,
            device_option = hu.cuda_do
        )
        self._terngrad.append(self._terngrad_gpu_op)
        self._terngrad_r_gpu_op = core.CreateOperator(
            'terngrad_r',
            ['compressed', 'decompressed'],
            [],
            is_add_to = 0,
            device_option = hu.cuda_do
        )
        self._terngrad_r.append(self._terngrad_r_gpu_op)
        self._terngrad_r_gpu_op = core.CreateOperator(
            'terngrad_r',
            ['compressed', 'decompressed'],
            [],
            is_add_to = 1,
            device_option = hu.cuda_do
        )
        self._terngrad_r.append(self._terngrad_r_gpu_op)
    def _zq_init(self,**kargs):
        self._comp_cpu = dict()
        self._comp_gpu = dict()
        self._comp_set = set()
        self._param = dict()
        self._terngrad_init()
        self._threshold = kargs['threshold']
        
    def _zq_malloc_xpu(self, name:str, cpu_size:int, gpu_size:int, cpu_type=torch.uint8, gpu_type=torch.uint8):
        '''
        assume you have checked name not in self._comp_set
        '''
        # if name in self._comp_set():
        #     return
        self._comp_cpu[name] = torch.zeros(cpu_size,dtype=cpu_type)
        if gpu_type == torch.uint8:
            self._comp_gpu[name] = torch.cuda.ByteTensor(gpu_size)
        elif gpu_type == torch.float32:
            self._comp_gpu[name] = torch.cuda.FloatTensor(gpu_size)
        else:
            print("_zq_malloc_xpu: unknown gpu_type: {}, only support torch.uint8 or torch.float32".format(gpu_type))
            raise(ValueError)

    def __init__(self, params, named_parameters, compression,
                 backward_passes_per_step=1,**kargs):
        super(self.__class__, self).__init__(params)
        self._compression = compression

        self._zq_init(**kargs)
        if named_parameters is not None:
            named_parameters = list(named_parameters)
        else:
            named_parameters = []

        # make sure that named_parameters are tuples
        if any([not isinstance(p, tuple) for p in named_parameters]):
            raise ValueError('named_parameters should be a sequence of '
                             'tuples (name, parameter), usually produced by '
                             'model.named_parameters().')

        dups = _DistributedOptimizer.find_duplicates([k for k, _ in named_parameters])
        if len(dups) > 0:
            raise ValueError('Parameter names in named_parameters must be unique. '
                             'Found duplicates: %s' % ', '.join(dups))

        if len(named_parameters) > 0:
            self._parameter_names = {v: k for k, v
                                     in sorted(named_parameters)}
        else:
            self._parameter_names = {v: 'allreduce.noname.%s' % i
                                     for param_group in self.param_groups
                                     for i, v in enumerate(param_group['params'])}
        self.backward_passes_per_step = backward_passes_per_step
        self._allreduce_delay = {v: self.backward_passes_per_step
                                 for _, v in sorted(named_parameters)}
        self._handles = {}
        self._2nd_handles = {}
        self._3rd_handles = {}
        self._grad_accs = []
        self._requires_update = set()
        if size() > 1:
            self._register_hooks()

    @staticmethod
    def find_duplicates(lst):
        seen = set()
        dups = set()
        for el in lst:
            if el in seen:
                dups.add(el)
            seen.add(el)
        return dups

    def set_backward_passes_per_step(self, passes):
        self.backward_passes_per_step = passes
        for p in self._allreduce_delay:
            self._allreduce_delay[p] = self.backward_passes_per_step

    def _register_hooks(self):
        for param_group in self.param_groups:
            for p in param_group['params']:
                if p.requires_grad:
                    p.grad = p.data.new(p.size()).zero_()
                    self._requires_update.add(p)
                    p_tmp = p.expand_as(p)
                    grad_acc = p_tmp.grad_fn.next_functions[0][0]
                    grad_acc.register_hook(self._make_hook(p))
                    self._grad_accs.append(grad_acc)
    
    def original_size_2_compressed_size(self, size:int, algorithm_name='terngrad', **d):
        '''
        size: number of float
        return: number of uint8/byte
        '''
        if algorithm_name=='terngrad':
            data_per_byte = 8 / d['bitwidth']
            compressed_size =  10 + (size + data_per_byte - 1) // data_per_byte
            auxiliary_size = 4*size
            return int(compressed_size),int(auxiliary_size)
        else:
            print("unknown algorithm_name:{}".format(algorithm_name))
            raise(ValueError)

    def is_root(self):
        '''
        set node 0 as global root
        '''
        return rank()==self.root_id()
    def root_id(self):
        return 0
    


    def _my_gather_reduce_bcast_grad_async_confirm(self, p):
        name = self._parameter_names.get(p)
        tensor = p.grad
        numel = tensor.numel()
        tensor = tensor.reshape(tensor.numel())
        compressed_size, auxiliary_size = self.original_size_2_compressed_size(numel, 'terngrad', bitwidth=2)
        # print("_my_gather_reduce_bcast_grad_async::numel={}, \
        #     compressed_size={}, auxiliary_size={}".format(
        #         numel, compressed_size, auxiliary_size
        #     ))
        CPU_size = numel
        CPU_type = torch.float32
        GPU_size = auxiliary_size
        if name not in self._comp_set:
            self._zq_malloc_xpu(name, CPU_size, GPU_size, CPU_type)
        B = self._comp_gpu[name]
        C = self._comp_cpu[name]
        # print("name:{}\ttensorO:".format(name), tensor[:min(5,len(tensor))])
        workspace.FeedBlob('original', tensor)
        workspace.FeedBlob('compressed', B)
        #run terngrad_op with random==1 
        workspace.RunOperatorOnce(self._terngrad[self._param['terngrad']['random']]) 
        workspace.FeedBlob('decompressed', tensor)
        #run terngrad_r_op with is_add_to = 0
        workspace.RunOperatorOnce(self._terngrad_r[0])
        # print("name:{}\ttensorD:".format(name), tensor[:min(5,len(tensor))])
        C.copy_(tensor)
        handle = allreduce_async(C[:1], average=True, name=name)
        return handle,0




    #horovod run over it.
    def _my_gather_reduce_bcast_grad_async(self, p):
        '''
        self-implemented.
        gradients A on GPU: float[N]
        rank ~0(not root): compress to B on GPU: uint8[M]  (M maybe larger than compressed size, because compression need auxiliary space)
        rank ~0: copy it out to CPU as C: uint8[compressed_size]
        rank 0: gather need number-of-machine(R)-1 times space as D on CPU: uint8[R][compressed_size]
        rank 0: for each received compressed gradients, copy it B on GPU(assert M is enough for decompression needed auxiliary space), 
            decompress it and reduce it on its original A
        rank 0: compress to B on GPU: uint8[M]
        rank 0: copy compressed B to D[0]
        rank 0: bcast D[0] to all other nodes             
        rank ~0: recieve compressed data from root by bcast, -> C
        rank ~0: copy C on CPU to B on GPU
        rank ~0: decompress B to A
        '''
        '''
        rank0: 
            GPU:uint8[auxiliary_size]
            CPU:R * uint8[compressed_size]
        rank~0:
            GPU:uint8[auxiliary_size]
            CPU:R * uint8[compressed_size]
        '''
        name = self._parameter_names.get(p)
        tensor = p.grad
        numel = tensor.numel()
        if numel < self._threshold:
            handle = allreduce_async_(tensor,average=True,name=name)
            return handle,1

        tensor = tensor.reshape(numel)
        is_root = self.is_root()
        compressed_size, auxiliary_size = self.original_size_2_compressed_size(numel, 'terngrad', bitwidth=2)
        # if name not in self._comp_set:
        #     if is_root:
        #         CPU_size = size()*compressed_size
        #     else:
        #         CPU_size = compressed_size
        #     GPU_size = auxiliary_size
        #     self._zq_malloc_xpu(name, CPU_size, GPU_size)
        CPU_size = size()*compressed_size
        GPU_size = auxiliary_size
        self._zq_malloc_xpu(name, CPU_size, GPU_size)

        B = self._comp_gpu[name]
        C = self._comp_cpu[name]
        workspace.FeedBlob('original', tensor)
        workspace.FeedBlob('compressed', B[:compressed_size])
        workspace.FeedBlob('decompressed',tensor)
        if not self.is_root():
            workspace.RunOperatorOnce(self._terngrad[self._param['terngrad']['random']])
            C[:compressed_size].copy_(B[:compressed_size])
        D=torch.zeros(CPU_size*16, dtype=torch.uint8)
        handle = gather_async_(
            C, 
            D, 
            self.root_id(),
            num_elem = compressed_size,
            name = name,
            batchid=0
        )
        return handle, 0

    def _allreduce_grad_async(self, p):
        name = self._parameter_names.get(p)
        tensor = p.grad
        tensor = tensor.reshape(tensor.numel()) # zq: reshape it to 1 dimension
        tensor_compressed, ctx = self._compression.compress(tensor)

        handle = allreduce_async_(tensor_compressed, average=True, name=name)
        return handle, ctx
    
    def _gather_grad_async(self,p):
        name = self._parameter_names.get(p)
        tensor = p.grad
        handle = gather_async_(tensor,)
    

    def _make_hook(self, p):
        def hook(*ignore):
            if p in self._handles and self._handles[p][0] is not None:
                if self._allreduce_delay[p] <= 0:
                    raise AssertionError(
                        "Gradients were computed more than "
                        "backward_passes_per_step times before call "
                        "to step(). Increase backward_passes_per_step to "
                        "accumulate gradients locally.")
            assert not p.grad.requires_grad
            assert self._allreduce_delay[p] > 0
            handle, ctx = None, None
            self._allreduce_delay[p] -= 1
            if self._allreduce_delay[p] == 0:
                #horovod run over it to call a
                handle, ctx = self._my_gather_reduce_bcast_grad_async(p)
            self._handles[p] = (handle, ctx)
        return hook

    def get_attrs_from_p(self, p, **d):
        name = self._parameter_names.get(p)
        tensor = p.grad
        numel = tensor.numel()
        tensor = tensor.reshape(numel)
        compressed_size,auxiliary_size = self.original_size_2_compressed_size(
            numel, 
            'terngrad',
            bitwidth = self._param['terngrad']['bitwidth']
        )
        return name,tensor,numel,compressed_size,auxiliary_size


    #horovod run over it.
    def synchronize(self):
        missing_p = self._requires_update - set(self._handles.keys())
        for p in missing_p:
            handle, ctx = self._my_gather_reduce_bcast_grad_async(p)
            print("horovod::torch::_DistributedOptimizer::synchronize::loop1::p,handle,ctx:",handle,ctx)
            self._handles[p] = (handle, ctx)

        for p, value in self._handles.items():
            handle, ctx = value
            if handle is None:
                handle, ctx = self._my_gather_reduce_bcast_grad_async(p)
                print("horovod::torch::_DistributedOptimizer::synchronize::loop2::p,handle,ctx:",handle,ctx)
                self._handles[p] = (handle, ctx)

        for p,(handle, ctx) in self._handles.items():
            C = synchronize(handle)
            if ctx==1: #original part
                self._allreduce_delay[p] = self.backward_passes_per_step
                p.grad.set_(C)
                continue
            #decompress
            self._allreduce_delay[p] = self.backward_passes_per_step
            name, tensor, numel, compressed_size, auxiliary_size = self.get_attrs_from_p(p, algorithm='terngrad', bitwidth=2)
            B = self._comp_gpu[name]
            workspace.FeedBlob('original', tensor)
            workspace.FeedBlob('compressed',B[:compressed_size])
            workspace.FeedBlob('decompressed',tensor)
            if self.is_root():
                for i in range(size()):
                    if i != rank():
                        head = compressed_size * i
                        tail = head + compressed_size
                        B[:compressed_size].copy_(C[head:tail])
                        # workspace.RunOperatorOnce(self._terngrad_r[1])
                workspace.RunOperatorOnce(self._terngrad[self._param['terngrad']['random']])
                C[:compressed_size].copy_(B[:compressed_size])
            handle = cbroadcast_async_(
                C,
                self.root_id(),
                num_elem = compressed_size,
                name = name,
                batchid = 0
            )
            self._2nd_handles[p] = (handle,0)
        self._handles.clear()
        
        for p,(handle,ctx) in self._2nd_handles.items():
            name, tensor, numel, compressed_size, auxiliary_size = self.get_attrs_from_p(p, algorihtm='terngrad', bitwidth=2)
            C = synchronize(handle)
            B = self._comp_gpu[name]
            if not self.is_root():
                B[:compressed_size].copy_(C[:compressed_size])
                workspace.RunOperatorOnce(self._terngrad_r[0])
            # to calc average value.
        self._2nd_handles.clear()

        


    #horovod run over it.
    def synchronize_bak(self):
        missing_p = self._requires_update - set(self._handles.keys())
        for p in missing_p:
            handle, ctx = self._my_gather_reduce_bcast_grad_async(p)
            print("horovod::torch::_DistributedOptimizer::synchronize::loop1::p,handle,ctx:",handle,ctx)
            self._handles[p] = (handle, ctx)

        for p, value in self._handles.items():
            handle, ctx = value
            if handle is None:
                handle, ctx = self._my_gather_reduce_bcast_grad_async(p)
                print("horovod::torch::_DistributedOptimizer::synchronize::loop2::p,handle,ctx:",handle,ctx)
                self._handles[p] = (handle, ctx)
        for p, (handle, _) in self._handles.items():
            output = synchronize(handle)
            self._allreduce_delay[p] = self.backward_passes_per_step
            shape = p.grad.shape
            p.grad.set_(self._compression.decompress(output, ctx))
            p.grad.set_(p.grad.reshape(shape))  # zq: reshape it back to original shape
            print("horovod::torch::_DistributedOptimizer::synchronize::loop3::p,ctx:",ctx)
        self._handles.clear()

    #horovod run over it.
    def step(self, closure=None):
        # print("horovod::torch::_DistributedOptimzier::step()")
        self.synchronize()
        r'''
        torch.optim.Optimzier.step():
        Performs a single optimization step(parameter update).

        Auguments:
            closure(callable): A closure that reevaluates the model and
                returns the loss. Optinal for most optimzers.
        '''
        return super(self.__class__, self).step(closure)



def DistributedOptimizer(optimizer, named_parameters=None,
                         compression=Compression.none,
                         backward_passes_per_step=1,
                         **kargs):
    """
    An optimizer that wraps another torch.optim.Optimizer, using an allreduce to
    average gradient values before applying gradients to model weights.

    Allreduce operations are executed after each gradient is computed by `loss.backward()`
    in parallel with each other. The `step()` method ensures that all allreduce operations are
    finished before applying gradients to the model.

    DistributedOptimizer exposes the `synchronize()` method, which forces allreduce operations
    to finish before continuing the execution. It's useful in conjunction with gradient
    clipping, or other operations that modify gradients in place before `step()` is executed.

    Example of gradient clipping:
    ```
    output = model(data)
    loss = F.nll_loss(output, target)
    loss.backward()
    optimizer.synchronize()
    torch.nn.utils.clip_grad_norm(model.parameters(), args.clip)
    optimizer.step()
    ```

    Arguments:
        optimizer: Optimizer to use for computing gradients and applying updates.
        named_parameters: A mapping between parameter names and values. Used for naming of
                          allreduce operations. Typically just `model.named_parameters()`.
        compression: Compression algorithm used during allreduce to reduce the amount
                     of data sent during the each parameter update step.  Defaults to
                     not using compression.
        backward_passes_per_step: Number of expected backward passes to perform
                                  before calling step()/synchronize(). This
                                  allows accumulating gradients over multiple
                                  mini-batches before executing averaging and
                                  applying them.
    """
    # We dynamically create a new class that inherits from the optimizer that was passed in.
    # The goal is to override the `step()` method with an allreduce implementation.
    cls = type(optimizer.__class__.__name__, (optimizer.__class__,),
               dict(_DistributedOptimizer.__dict__))
    return cls(optimizer.param_groups, named_parameters,
               compression, backward_passes_per_step, **kargs)


def broadcast_parameters(params, root_rank):
    """
    Broadcasts the parameters from root rank to all other processes.
    Typical usage is to broadcast the `model.state_dict()`,
    `model.named_parameters()`, or `model.parameters()`.

    Arguments:
        params: One of the following:
            - list of parameters to broadcast
            - dict of parameters to broadcast
        root_rank: The rank of the process from which parameters will be
                   broadcasted to all other processes.
    """
    if isinstance(params, dict):
        params = sorted(params.items())
    elif isinstance(params, list):
        # support both named_parameters() and regular parameters()
        params = [p if isinstance(p, tuple) else (None, p) for p in params]
    else:
        raise ValueError('invalid params of type: %s' % type(params))

    # Run asynchronous broadcasts.
    handles = []
    for name, p in params:
        handle = broadcast_async_(p, root_rank, name)
        handles.append(handle)

    # Wait for completion.
    for handle in handles:
        synchronize(handle)


def broadcast_optimizer_state(optimizer, root_rank):
    """
    Broadcasts an optimizer state from root rank to all other processes.

    Arguments:
        optimizer: An optimizer.
        root_rank: The rank of the process from which the optimizer will be
                   broadcasted to all other processes.
    """
    if isinstance(optimizer, torch.optim.LBFGS):
        # TODO(travis): L-BFGS cannot be easily supported without serializing
        # the entire state_dict, as its structure is deeply nested and contains
        # None type parameter values
        raise ValueError('cannot broadcast torch.optim.LBFGS state')

    state_dict = optimizer.state_dict()

    # Newly created optimizers will not have their state initialized, so
    # do that initialization here
    if len(state_dict['state']) == 0:
        for group in optimizer.param_groups:
            for p in group['params']:
                p.grad = p.data.new(p.size()).zero_()
        # This function accepts a torch.optim.Optimizer or a DistributedOptimizer
        # wrapped around a torch optimizer. Calling step() with a DistributedOptimizer
        # forces allreduce on all model parameters, which will result in deadlock
        # unless every rank calls step(). Therefore, to finish state initialization
        # only call optimizer.step() with a torch.optim.Optimizer.
        if optimizer.__module__ == DistributedOptimizer.__module__:
            super(optimizer.__class__, optimizer).step()
        else:
            optimizer.step()
        state_dict = optimizer.state_dict()

    # If the state_dict is still empty after initialization, then
    # the optimizer is stateless, and there is nothing to broadcast.
    # Furthermore, attempting to access the state dict would result in
    # an error.
    if len(state_dict['state']) == 0:
        return

    params = []
    callbacks = {}
    occurrences = collections.defaultdict(int)

    # Returns the full type structure of the possibly nested objects for recursive casting back
    def _get_types(x):
        if isinstance(x, collections.Iterable):
            return type(x), [_get_types(xi) for xi in x]
        else:
            return type(x)

    # Casts an object encoded in a tensor back into its original type and subtypes
    def _recursive_cast(x, dtype):
        if isinstance(dtype, tuple):
            t, dtypes = dtype
            x = t(x)
            return t([_recursive_cast(x[i], dtypes[i]) for i in range(len(x))])
        else:
            return dtype(x)

    # Some optimizer parameters may be represented as scalars instead of
    # tensors.  In such cases, we need to wrap the scalar in a tensor, then
    # broadcast, then update the appropriate value in the state_dict with the
    # new unwrapped scalar value via a callback.
    def _create_callback(pid, name, t, p):
        def _from_tensor():
            state_dict['state'][pid][name] = t(p.numpy()[0])
        return _from_tensor

    def _create_option_callback(index, option_key, option_tensor, dtypes):
        def _from_tensor():
            optimizer.param_groups[index][option_key] = _recursive_cast(option_tensor.numpy()[0], dtypes)
        return _from_tensor

    # Param groups are an ordered list, normally there is only one per model,
    # but users can add additional param groups for example to train
    # previously frozen layers
    for index, group in enumerate(state_dict['param_groups']):
        # Broadcast options like learning rate
        for option_key, option_value in group.items():
            if option_key == 'params':
                continue

            # Options like the learning rate are scalar, and need to be wrapped in tensors
            key = '%s.%d' % (option_key, index)
            dtypes = _get_types(option_value)
            option_tensor = torch.Tensor([option_value])
            callbacks[key] = _create_option_callback(index, option_key, option_tensor, dtypes)
            params.append((key, option_tensor))

        # The params list here is ordered by the layers in the model
        for pid in group['params']:
            param_state = state_dict['state'][pid]
            for name, p in param_state.items():
                # Some parameter names may appear more than once, in which
                # case we ensure they have a unique identifier defined by
                # their order
                occurrences[name] += 1
                key = '%s.%d' % (str(name), occurrences[name])

                if not torch.is_tensor(p):
                    # Wrap the scalar in a FloatTensor, and remember its type
                    # so we can cast it back after unwrapping
                    t = type(p)
                    p = torch.Tensor([p])
                    callbacks[key] = _create_callback(pid, name, t, p)

                params.append((key, p))

    # Synchronized broadcast of all parameters
    broadcast_parameters(params, root_rank)

    # Post-broadcast clenaup for non-tensor parameters
    for key, p in params:
        if key in callbacks:
            callbacks[key]()
